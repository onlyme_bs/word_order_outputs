bleu_exp2:
	@for number in 20 64 512 ; do \
        python bleu.py ./exp2/TEST_rama_beam_$$number.txt ./exp2/test_rama.trg.new -W ignore; \
	done
bleu_exp3:
	@for number in 20 64 512 ; do \
        python bleu.py ./exp3/TEST_wr_beam_$$number.txt ./exp3/test_wr.trg.new -W ignore; \
	done
bleu_lstm_r1:
	python bleu.py ./lstm_run_1/TEST_POETRY.txt ./lstm_run_1/test.trg.10K -W ignore
	python custom_score.py -s ./lstm_run_1/TEST_POETRY.txt -r ./lstm_run_1/test.trg.10K -o ./lstm_run_1/detailed.csv
