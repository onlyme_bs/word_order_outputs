# coding: utf-8


import re
import numpy as np


# In[64]:

regex1 = re.compile('[ ]+')


# In[72]:

def Pairwise_Metric(ground_truth, predicted):
    # Find the skip bigram pairs
    list_gt = regex1.split(ground_truth)
    list_pred = regex1.split(predicted)
    skip_bigrams_gt = [(list_gt[i], list_gt[j]) for i in range(len(list_gt)) for j in range(i+1, len(list_gt))]
    skip_bigrams_pred = [(list_pred[i], list_pred[j]) for i in range(len(list_pred)) for j in range(i+1, len(list_pred))]

    intersect_prec = [i for i in skip_bigrams_pred if i in skip_bigrams_gt]
    intersect_recl = [i for i in skip_bigrams_gt if i in skip_bigrams_pred]

    # Calculate and return precision, recall, f-score
    if len(skip_bigrams_pred) > 0:
        P = len(intersect_prec)*1.0/len(skip_bigrams_pred)
    else:
        P = 0 # IS THIS CORRECT??
    
    if len(skip_bigrams_gt) > 0:
        R = len(intersect_recl)*1.0/len(skip_bigrams_gt)
    else:
        # If the ground truth is empty, there is no point
        # in considering that in calculation
        return None
        
    try:
        F = 2*P*R/(P+R)
    except ZeroDivisionError:
        F = 0.0

    # for kendell Tau
    skipSKipped = [i for i in skip_bigrams_pred if i not in skip_bigrams_gt]
    try:
        tau = 1 - (1.0*len(skipSKipped))/len(skip_bigrams_pred)
    except ZeroDivisionError:
        tau = 0
    
    return P, R, F, tau

def __lcs(a, b):
    # The dynamic programming algo for longest common subsequence
    lengths = np.zeros((len(a) + 1, len(b) + 1))

    # row 0 and column 0 are initialized to 0 already
    for i, x in enumerate(a):
        for j, y in enumerate(b):
            if x == y:
                lengths[i+1, j+1] = lengths[i, j] + 1
            else:
                lengths[i+1, j+1] = max(lengths[i+1, j], lengths[i, j+1])

    return lengths[-1, -1]
def LSR(ground_truth, predicted):
    # Longest sequence ratio
    list_gt = regex1.split(ground_truth.strip())
    list_pred = regex1.split(predicted.strip())
    lcs_match = __lcs(list_gt, list_pred)
    # LSR = lcs_match/len(list_pred)

    # Calculate and return precision, recall, f-score
    if len(list_pred) > 0:
        P = lcs_match/len(list_pred)
    else:
        P = 0

    if len(list_gt) > 0:
        R = lcs_match/len(list_gt)
    else:
        return None

    if (P <= 0) and (R <= 0):
        F = 0
    else:
        F = 2*P*R/(P+R)

    return P,R,F

def Perfect_Match(ground_truth, predicted):
    # Perfect match b/w ground_truth sequence and predicted sequence
    list_gt = regex1.split(ground_truth.strip())
    list_pred = regex1.split(predicted.strip())
    pm = 0
    pred_len = len(list_pred)
    for i, gx in enumerate(list_gt):
        if(i >= pred_len):
            break
        if gx == list_pred[i]:
            pm += 1
    pm = pm/float(len(list_pred))
    return pm


# In[73]:

if __name__ == "__main__":
	sentence_gt = '▁guhya m ▁arTam ▁mAmak eByaH ▁bravImi ▁mAtA mah aH ▁aham ▁BavatA m ▁prakASa H'
	sentence_pred = 'm aH eByaH ▁guhya ▁arTam ▁mAmak mah ▁aham  ▁mAtA ▁bravImi ▁BavatA m ▁prakASa H'
	print('Pairwise Metric(Precision, recall , F-score): ', Pairwise_Metric(sentence_gt, sentence_pred))
	print('Longest Sequence Ratio: ', LSR(sentence_gt, sentence_pred))
	print('Perfect Match Ratio: ', Perfect_Match(sentence_gt, sentence_pred))


