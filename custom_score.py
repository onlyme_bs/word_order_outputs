#!/usr/bin/env python3
# Copyright (c) 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the LICENSE file in
# the root directory of this source tree. An additional grant of patent rights
# can be found in the PATENTS file in the same directory.
#

import argparse
import os, csv
import sys
from nltk.translate.bleu_score import sentence_bleu
from scripts.bleu import compute_bleu
import numpy as np

# from fairseq import bleu, dictionary, tokenizer
from tqdm import tqdm
from metrics import *

def main():
    parser = argparse.ArgumentParser(description='Command-line script for Special Metrics scoring.')
    parser.add_argument('-s', '--sys', default='-', help='system output')
    parser.add_argument('-r', '--ref', required=True, help='references')
    parser.add_argument('-o', '--out', required=True, help='output file')
    # parser.add_argument('-n', '--npy', required=True, help='numpy file')

    args = parser.parse_args()

    assert args.sys == '-' or os.path.exists(args.sys), \
        "System output file {} does not exist".format(args.sys)
    assert os.path.exists(args.ref), \
        "Reference file {} does not exist".format(args.ref)

    import re
    regex_space = re.compile(r'[ ]+')
    with open(args.sys, 'r') as fl:
        pred_lines = fl.read().split('\n')
    
    with open(args.ref, 'r') as fl:
        true_lines = fl.read().split('\n')
    
    print('Predicted: %d samples' % len(pred_lines))
    print('In GT: %d samples' % len(true_lines))
    
    num_pred = len(pred_lines)
    score_list = []
    with open(args.out, 'w') as fl:
        csv_log = csv.writer(fl)
        csv_log.writerow(['PS', 'GT', 'PWM', 'LSR', 'PRM', 'TAU', 'BLEU'])
        for i in tqdm(range(num_pred)):
            try:
                true_lines[i] = true_lines[i].replace('</s>', '')
                pred_lines[i] = pred_lines[i].replace('</s>', '')     
                           
                true_lines[i] = true_lines[i].strip()
                pred_lines[i] = pred_lines[i].strip()
                pwm = Pairwise_Metric(true_lines[i], pred_lines[i])                
                lsr = LSR(true_lines[i], pred_lines[i])
                prm = Perfect_Match(true_lines[i], pred_lines[i])
                
                if (pwm is None) or (lsr is None):
                    continue

                #print(score_list)
                #if(i == 150):
                #    break
                
                # fl.write('PS: ' + pred_lines[i] + '\n')
                # fl.write('GT: ' + true_lines[i] + '\n')
                # fl.write('PWM: ' + str(pwm[0]) + ',' + str(pwm[1]) + ',' +str(pwm[2])+ '\n')
                # fl.write('LSR: ' + str(lsr[0]) + ',' + str(lsr[1]) + ',' +str(lsr[2])+ '\n')
                # fl.write('PRM: ' + str(prm) + '\n')


                tau = None
                if len(pwm) > 3:
                    # TAU Score included
                    tau = pwm[3]
                    # fl.write('TAU: ' + str(tau) + '\n')
                    score_list.append([pwm[0], pwm[1], pwm[2], lsr[0], lsr[1], lsr[2], prm, tau])
                else:
                    # TAU Score missing
                    score_list.append([pwm[0], pwm[1], pwm[2], lsr[0], lsr[1], lsr[2], prm])
                # print(compute_bleu([true_lines[i].split(" ")], [pred_lines[i]])[0])
                # exit(0)
                csv_log.writerow([pred_lines[i], true_lines[i],
                                  '%f:%f:%f' % (pwm[0], pwm[1], pwm[2]),
                                  '%f:%f:%f' % (lsr[0], lsr[1], lsr[2]),
                                  '%f' % prm,
                                  '%f' % tau,
                                  #'%f' % sentence_bleu([true_lines[i]], pred_lines[i]),
                                  '%f' % compute_bleu([true_lines[i].split(" ")], [pred_lines[i]])[0]
                                  ])
                
            except ZeroDivisionError as e:
                raise e
                print(',',i, true_lines[i], pred_lines[i])
    score_arr = np.asarray(score_list)
    # np.save(args.npy, score_arr)
    mean_scores = np.mean(score_arr, axis=0)

    print()
    print('===============================')
    print('PRECISION | RECALL | F-SCORE')
    print('Pairwise Match: %0.4f, %0.4f, %0.4f' % (mean_scores[0], mean_scores[1], mean_scores[2]))
    print('Longest Sequence Match: %0.4f, %0.4f, %0.4f' % (mean_scores[3], mean_scores[4], mean_scores[5]))
    print('Perfect Match: %0.4f' % mean_scores[6])
    try:
        print('Kendall-Tau Dist.: %0.4f' % mean_scores[7])
    except Exception as e:
        print(e)
    print('===============================')

if __name__ == '__main__':
    main()